package CustomBots;
/*****
 * CSP
 *****/

import snakes.*;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Random;

public class BotForwardChecking implements Bot {

    private final Random rnd = new Random();
    private static final Direction[] DIRECTIONS = new Direction[]{Direction.UP, Direction.DOWN, Direction.LEFT, Direction.RIGHT};

    private double calculate_manhattan_d(Coordinate a, Coordinate b) {
        return Math.sqrt(Math.abs(a.x - b.x) + Math.abs(a.y - b.y));
    }

    @Override
    public Direction chooseDirection(Snake snake, Snake opponent, Coordinate board_size, Coordinate apple) {
        Coordinate head = snake.getHead();
        Coordinate head_opponent = opponent.getHead();

        Direction[] domain1 = forward_ch(snake, opponent, board_size, head);

        //en route to apple; manhattan
        if (domain1.length > 0) {
            double min_distance_apple = Math.max(board_size.x, board_size.y) + 1;
            Direction shortest_dir_apple = null;

            final Coordinate body_op = headless_body(opponent);
            Direction[] op_possible_moves = get_nonlethal_directions(opponent, head_opponent, body_op, opponent, board_size);

            for (Direction dir : domain1) {
                double m_distance = calculate_manhattan_d(head.moveTo(dir), apple);

                Snake new_s = snake.clone();
                new_s.moveTo(dir, false);

                //for each possible movement of the opponent check if a collision occurs
                boolean no_collision = true;
                for (Direction d_op : op_possible_moves) {
                    Snake new_op = opponent.clone();
                    new_op.moveTo(d_op, false);

                    no_collision = no_collision & !new_op.elements.contains(new_s.getHead());
                }
                if (m_distance < min_distance_apple && no_collision) {
                    min_distance_apple = m_distance;
                    shortest_dir_apple = dir;
                }
            }

                double min_distance_apple_op = calculate_manhattan_d(head_opponent, apple);

                if (min_distance_apple_op > min_distance_apple)
                    return shortest_dir_apple;
                else
                    return domain1[rnd.nextInt(domain1.length)];
            } else{
                Direction[] next_move = go_forward(snake, head);
                return next_move[rnd.nextInt(next_move.length)];
            }
        }


    /**
     * not taking into account the movement of the opponent
     **/
    private static Direction[] forward_ch(Snake snake, Snake opponent, Coordinate board_size, Coordinate head) {
        final Coordinate body_s = headless_body(snake); //first body element

        //domain1 - direct possible directions that do not directly lead to death
        Direction[] domain1 = get_nonlethal_directions(snake, head, body_s, opponent, board_size);

        //3-step forward checking algorithm
        for(Direction dir : domain1){
            Coordinate head_clone = snake.getHead(); //first body element
            Coordinate temp = head.moveTo(dir); //new head
            //domain2 - every direction (after taking a step from domain1) that does not lead to death - first step
            Direction[] domain2 = get_nonlethal_directions(snake, temp, head_clone, opponent, board_size);

            for(Direction direct : domain2){
                Coordinate temp2 = temp.moveTo(direct);
                //domain3 - every direction (after taking a step from domain2) that does not lead to death - second step
                Direction[] domain3 = get_nonlethal_directions(snake, temp2, temp, opponent, board_size);

                for(Direction di : domain3){
                    Coordinate temp3 = temp2.moveTo(di);
                    //domain4 - every direction (after taking a step from domain3) that does not lead to death - third step
                    Direction[] domain4 = get_nonlethal_directions(snake, temp3, temp2, opponent, board_size);
                    if(domain4.length == 0) {
                        domain3 = Arrays.stream(domain3)
                                .filter(d -> !(d == di))
                                .toArray(Direction[]::new);
                    }
                }
                if(domain3.length == 0) {
                    domain2 = Arrays.stream(domain2)
                            .filter(d -> !(d == direct))
                            .toArray(Direction[]::new);
                }
            }
            //if second step leads to death, remove from domain1
            if(domain2.length == 0) {
                domain1 = Arrays.stream(domain1)
                        .filter(d -> !(d == dir))
                        .toArray(Direction[]::new);
            }
        }
        return domain1;
    }

    private static Coordinate headless_body(Snake snake) {
        Coordinate headless = null;
        if (snake.body.size() >= 2) {
            Iterator<Coordinate> it = snake.body.iterator();
            it.next();
            headless = it.next();
        }
        return headless;
    }


    private static Direction[] get_nonlethal_directions
            (Snake snake, Coordinate head, Coordinate afterHead, Snake opponent, Coordinate board_size) {
        Direction[] domain = Arrays.stream(DIRECTIONS)
                .filter(d -> !head.moveTo(d).equals(afterHead))                //don't go back
                .filter(d -> head.moveTo(d).inBounds(board_size))             //don't leave maze
                .filter(d -> !opponent.elements.contains(head.moveTo(d)))    //don't collide with opponent...
                .filter(d -> !snake.elements.contains(head.moveTo(d)))      //and yourself
                .toArray(Direction[]::new);

        return domain;
    }

    private static Direction[] go_forward(Snake snake, Coordinate head){
        final Coordinate body = headless_body(snake);

        Direction[] go_forward = Arrays.stream(DIRECTIONS)
                .filter(d -> !head.moveTo(d).equals(body))
                .toArray(Direction[]::new);

        return go_forward;
    }
}
