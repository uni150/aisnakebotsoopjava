package CustomBots;
/*****
 * breadth-first search algorithm
 *****/
import snakes.Bot;
import snakes.Coordinate;
import snakes.Direction;
import snakes.Snake;

import java.util.*;

public class BotBFS implements Bot {
    private static final Direction[] DIRECTIONS = new Direction[]{Direction.UP, Direction.DOWN, Direction.LEFT, Direction.RIGHT};

    private int moves = 0;

    /*
     * Manhattan distance measures the distance between two coordinates in a grid-based environment
     * by calculating the sum of the absolute differences of their coordinates along each dimension of the grid
     */
    private static int Manhattan(Coordinate a, Coordinate b) {
        return (int) Math.sqrt(Math.abs(a.x - b.x) + Math.abs(a.y - b.y));
    }

    /*
     * SampleBot code snippet used from the demo
     * https://github.com/BeLuckyDaf/snakes-game-tutorial/blob/9af7401da32ee7b656087a5d49da77073ae02605/src/johndoe/SampleBot.java#L18
     */

    /**
     * @param snake    Your snake's body with coordinates for each segment
     * @param opponent Opponent snake's body with coordinates for each segment
     * @param mazeSize Size of the board
     * @param apple    Coordinate of an apple
     * @return Bot Direction
     */

    public Direction chooseDirection(Snake snake, Snake opponent, Coordinate mazeSize, Coordinate apple) {
        Coordinate head = snake.getHead();

        /*
         * Keeps track of the locations that have been visited by the snake
         */
        boolean[][] visitedSpaces = new boolean[mazeSize.x][mazeSize.y];
        visitedSpaces[head.x][head.y] = true; // Skip the head

        HashSet<Coordinate> headOpponent = new HashSet<>();
        for (Direction d : DIRECTIONS) {
            headOpponent.add(opponent.getHead().moveTo(d));
        }

        /*
         * Mark the positions on the board that are occupied by either of the snakes
         * and prevent the bot from considering these positions as valid options for its next move
         */
        for (int x = 0; x < mazeSize.x; x++) {
            for (int y = 0; y < mazeSize.y; y++) {
                Coordinate currentSpace = new Coordinate(x, y);
                if (snake.elements.contains(currentSpace) || opponent.elements.contains(currentSpace)) {
                    visitedSpaces[x][y] = true;
                }
            }
        }

        /*
         * Adds the current head position of the snake to the queue and as it traverses the grid,
         * it marks the location as visited and stores the previous location in a pastPoint HashMap
         */
        Map<Coordinate, Coordinate> previousPoints = new HashMap<>();
        previousPoints.put(head, null);
        Queue<Coordinate> queue = new LinkedList<>(); // The BFS algorithm requires a queue list to store the positions
        queue.offer(head); // The queue starts with the head
        visitedSpaces[head.x][head.y] = true; // Indicate that the current position of the snake's head has already been visited

        /*
         * Starting the main loop of the Breadth-first search algorithm
         */

        while (!queue.isEmpty()) {
            Coordinate current = queue.poll(); // Retrieves and removes the head of the queue
            for (Direction move : Direction.values()) { // Iterate over all 4 possible directions
                Coordinate newPos = current.moveTo(move); // Calculates the new position
                if (newPos.inBounds(mazeSize)) { // Check if new position in maze
                    if (!visitedSpaces[newPos.x][newPos.y]) { // Check if this positions has been visited before
                        queue.offer(newPos); // Add to queue if not visited before
                        visitedSpaces[newPos.x][newPos.y] = true; // Don't go to this position again.
                        if (!previousPoints.containsKey(newPos)) {
                            previousPoints.put(newPos, current); // Used to trace back the path later
                            // adds an entry in HashMap with new position as key and current position start as value
                        }
                    }
                }
                if (newPos == apple) {  // If new position is same as apple, apple found, brake loop
                    break;
                }
            }
        }

        LinkedList track = new LinkedList<>(); // Store the path from the apple to the snake's head
        if (apple != null) { // Checks if apple has been found
            if (previousPoints.containsKey(apple)) { // Checks if the apple is in path and stored
                track.add((apple));
                Collections.reverse(track); // Reverse the order of the elements in the list, so that it now contains the path from the apple to the snake's head
            }
        }

        /*
         * Determine the next direction to move
         */
        Direction move;
        if (track.size() == 1) { // Snake is already at the apple and there is no need to move
            while (track.get(track.size() - 1) != head) {
                track.add(previousPoints.get(track.getLast()));
            }
            move = moveTo(head, (Coordinate) track.get(track.size() - 2)); // Take current position of the snake's head and the next position in the path, and returns the next direction

        } else {
            /*
             * SampleBot code snippet used from the demo
             * https://github.com/BeLuckyDaf/snakes-game-tutorial/blob/9af7401da32ee7b656087a5d49da77073ae02605/src/johndoe/SampleBot.java#L29
             * Get the coordinate of the second element of the snake's body
             * to prevent going backwards
             */
            Coordinate afterHeadNotFinal = null;
            if (snake.body.size() >= 2) {
                Iterator<Coordinate> it = snake.body.iterator();
                it.next();
                afterHeadNotFinal = it.next();
            }
            final Coordinate afterHead = afterHeadNotFinal;

            /*
             * The only illegal move is going backwards. Here we are checking for not doing it
             */
            Direction[] validMoves = Arrays.stream(DIRECTIONS).filter(d -> !head.moveTo(d).equals(afterHead)) // Filter out the backwards move
                    .sorted().toArray(Direction[]::new);

            /* Just naïve greedy algorithm that tries not to die at each moment in time */
            Direction[] notLosing = Arrays.stream(validMoves).filter(d -> head.moveTo(d).inBounds(mazeSize)) // Don't leave maze
                    .filter(d -> !opponent.elements.contains(head.moveTo(d))) // Don't collide with opponent...
                    .filter(d -> !snake.elements.contains(head.moveTo(d))).filter(d -> !headOpponent.contains(head.moveTo(d))).filter(d -> !opponent.body.contains(head.moveTo(d))).sorted().toArray(Direction[]::new);

            if (notLosing.length > 0) { // If > 0 there are moves available for the snake to make that will not result in it losing the game
                // Calculate both the bot and opponents distances from heads to the apple
                int botDistanceToApple = Manhattan(head, apple);
                int opponentDistanceToApple = Manhattan(opponent.getHead(), apple);
                Random random = new Random(); 
                // Compare the distance of the opponent to the distance of the bot to the apple. If they are the same, 
                // the bot uses the random number generator to randomly select one of the two options
                if (opponentDistanceToApple > botDistanceToApple || opponentDistanceToApple == botDistanceToApple && random.nextInt() % 2 == 0) {
                    Arrays.sort(notLosing, new CompareDistance(apple, head));
                } else {
                    Coordinate opposite = new Coordinate(mazeSize.x / 2, mazeSize.y / 2); // Return to the center of the maze
                    Arrays.sort(notLosing, new CompareDistance(opposite, head));
                }
                move = notLosing[0];
            } else {
                move = validMoves[0];
            }
        }
        return move;
    }

    /**
     * determine the direction to move from one coordinate to another. It compares the coordinates,
     * and it returns the direction that will take the snake from the starting point to the next point
     *
     * @param start starting point on the grid
     * @param next  move to the next point on the grid
     * @return direction
     */
    public Direction moveTo(Coordinate start, Coordinate next) {
        final Coordinate vec = new Coordinate(next.x - start.x, next.y - start.y);
        if (vec.x > 0) {
            return Direction.RIGHT;
        } else if (vec.x < 0) {
            return Direction.LEFT;
        }
        if (vec.y > 0) {
            return Direction.UP;
        } else if (vec.y < 0) {
            return Direction.DOWN;
        }
        for (Direction direction : Direction.values())
            if (direction.dx == vec.x && direction.dy == vec.y) return direction;
        return null;
    }

    /*
     * Sorting is done so that the bot can move towards the apple or away from the opponent in the most efficient way,
     * by choosing the closest direction. It will be useful for the bot to move in the closest direction to the apple or
     * opponent snake since it will take less steps and thus less time to reach the target.
     */
    private static class CompareDistance implements Comparator<Direction> {
        private final Coordinate target;
        private final Coordinate head;

        public CompareDistance(Coordinate target, Coordinate head) {
            this.target = target;
            this.head = head;
        }

        public int compare(Direction a, Direction b) {
            return Integer.compare(Manhattan(head.moveTo(a), target), Manhattan(head.moveTo(b), target));
        }
    }
}