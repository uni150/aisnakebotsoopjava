package CustomBots;
/*****
 * almighty move - systematically go vertically through the board
 *****/

import snakes.*;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;

public class BotAlmightyMove implements Bot {
    private static final Direction[] DIRECTIONS = new Direction[]{Direction.UP, Direction.DOWN, Direction.LEFT, Direction.RIGHT};
    private static LinkedList <Coordinate> memory = new LinkedList<>();

    @Override
    public Direction chooseDirection(Snake snake, Snake opponent, Coordinate board_size, Coordinate apple) {
        Coordinate head = snake.getHead();

        memory.addLast(head);
        if (memory.size() > 15) {
            memory.removeFirst();
        }

        final Coordinate body_s = headless_body(snake); //first body element
        Direction[] domain = get_nonlethal_directions(snake, head, body_s, opponent, board_size);

        if (Arrays.stream(domain).anyMatch(d -> d == Direction.UP)) { //go up as long as possible
            return Direction.UP;
        }
        if (Arrays.stream(domain).anyMatch(d -> d == Direction.DOWN)) { //go down as long as possible
            return Direction.DOWN;
        }
        if (domain.length == 2) { //go left or right based on previous coordinates
            if(head.x > memory.getFirst().x)
                return Direction.RIGHT;
            else return Direction.LEFT;
        }
        else //corner, one decision
            return domain[0];

    }

    /**
     * @return the coordinate of the first body element
     */
    private static Coordinate headless_body(Snake snake) {
        Coordinate headless = null;
        if (snake.body.size() >= 2) {
            Iterator<Coordinate> it = snake.body.iterator();
            it.next();
            headless = it.next();
        }
        final Coordinate after = headless;
        return after;
    }

    /**
     * gives all directions with nonlethal outcome
     * lethal - collision with board, opponent, self; going backwards
     * @param snake         playing snake
     * @param head          snake's head coordinates
     * @param afterHead     first body element coordinates
     * @param opponent      opponent snake
     * @param board_size    board size
     * @return  nonlethal directions
     **/
    private static Direction[] get_nonlethal_directions
    (Snake snake, Coordinate head, Coordinate afterHead, Snake opponent, Coordinate board_size) {
        Direction[] domain = Arrays.stream(DIRECTIONS)
                .filter(d -> !head.moveTo(d).equals(afterHead))                //don't go back
                .filter(d -> head.moveTo(d).inBounds(board_size))             //don't leave maze
                .filter(d -> !opponent.elements.contains(head.moveTo(d)))    //don't collide with opponent...
                .filter(d -> !snake.elements.contains(head.moveTo(d)))      //and yourself
                .toArray(Direction[]::new);

        return domain;
    }
}

